package ptscheme

import (
	"strings"
	"testing"
)

func compareStringSlices(X, Y []string) bool {
	// check length
	if len(X) != len(Y) {
		return false
	}

	// check values
	for i, v := range X {
		if strings.Compare(v, Y[i]) != 0 {
			return false
		}
	}

	return true
}

func baseTokenizeTest(t *testing.T, input string, want []string) {
	got := Tokenize(input)
	if !compareStringSlices(want, got) {
		t.Errorf("want %s != got %s", want, got)
	}
}

// input: 1
// output: '1'
func TestTokenize1(t *testing.T) {
	baseTokenizeTest(t, "1", []string{"1"})
}

// input: abc
// output: 'abc'
func TestTokenize2(t *testing.T) {
	baseTokenizeTest(t, "abc", []string{"abc"})
}

// input: ()
// output: '(', ')'
func TestTokenize3(t *testing.T) {
	baseTokenizeTest(t, "()", []string{"(", ")"})
}

// input: 1 2 3 4 5
// output: '1', '2', '3', '4', '5'
func TestTokenize4(t *testing.T) {
	baseTokenizeTest(t, "1 2 3 4 5", []string{"1", "2", "3", "4", "5"})
}

// input: abc def hij klmn
// output: 'abc', 'def', 'hij', 'klmn'
func TestTokenize5(t *testing.T) {
	baseTokenizeTest(t, "abc def hij klmn", []string{"abc", "def", "hij", "klmn"})
}

// input: ( abc def )
// output: '(', 'abc', 'def', ')'
func TestTokenize6(t *testing.T) {
	baseTokenizeTest(t, "( abc def )", []string{"(", "abc", "def", ")"})
}

// (abc def)
// output: '(', 'abc', 'def', ')'
func TestTokenize7(t *testing.T) {
	baseTokenizeTest(t, "(abd def)", []string{"(", "abc", "def", ")"})
}

// (((((())))))
//  '(', '(', '(', '(', '(', '(', ')', ')', ')', ')', ')' ,')'
func TestTokenize8(t *testing.T) {
	baseTokenizeTest(t, "(((((())))))", []string{"(", "(", "(", "(", "(", "(", ")", ")", ")", ")", ")", ")"})
}
