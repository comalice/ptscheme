package ptscheme

import (
	"strconv"
)

func Parse(tokens []string) Integer {
	var object Integer
	object.value, _ = strconv.Atoi(tokens[0])
	return object
}
